import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get('hello')
  getHello(): string {
    return '<html><body><h1>Hello Yoonhong!</h1></body></html>';
  }
  @Post('Joshua')
  getJoshua(): string {
    return '<html><body><h1>Hello Joshua!</h1></body></html>';
  }
  @Get()
  get(): string {
    return '<html><body><h1>ยุนหงแฟนจริง!</h1></body></html>';
  }
}
