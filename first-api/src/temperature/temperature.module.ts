import { Module } from "@nestjs/common";
import { TemperatureController } from "./temperature.controller";
import { TemperatureService } from "./temperature.service";

@Module({
    imports: [TemperatureController],
    exports: [],
    controllers: [TemperatureService],
    providers: [],
})
export class TemperatureModule {}